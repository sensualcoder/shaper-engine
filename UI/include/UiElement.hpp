#ifndef UI_ELEMENT_HPP
#define UI_ELEMENT_HPP

class TCODConsole;

namespace engine
{
    struct pos_t;

    class UiElement
    {
        public:
            UiElement(bool visible = false);
            virtual ~UiElement() = default;

            void Show();
            void Hide();

            bool IsVisible() const;

            virtual void Render(TCODConsole* console, const pos_t& pos) const = 0;

        protected:
            bool visible_;
    };
}

#endif // UI_ELEMENT_HPP
