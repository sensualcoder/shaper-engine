#include "UiElement.hpp"

namespace engine
{
    UiElement::UiElement(bool visible)
        : visible_(visible)
    {
    }

    void UiElement::Show()
    {
        visible_ = true;
    }
    
    void UiElement::Hide()
    {
        visible_ = false;
    }

    bool UiElement::IsVisible() const
    {
        return visible_;
    }
}
