include(LibFindMacros)

libfind_pkg_check_modules(entt_PKGCONF entt)

find_path(entt_INCLUDE_DIR
    NAMES entt.hpp
    PATHS ${entt_PKGCONF_INCLUDE_DIRS}
)

libfind_process(entt)
