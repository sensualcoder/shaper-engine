// Core
// #include "Context.hpp"
#ifndef CONTEXT_HPP
#define CONTEXT_HPP

class TCODConsole;

namespace engine
{
    class Context
    {
        public:
            virtual ~Context() {}

            virtual void RegisterEvents() = 0;

            virtual void HandleInput() = 0;

            virtual void Update() = 0;

            virtual void Render(TCODConsole* parent) const = 0;
    };
}

#endif // CONTEXT_HPP

// #include "Engine.hpp"
#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <memory>

// #include "Configuration.hpp"
#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>

namespace engine
{
    struct Configuration
    {
        int width;
        int height;
        std::string title;
        bool fullscreen;
    };

    void SaveConfig(std::string filename, Configuration& config);
    Configuration LoadConfig(std::string filename);
}

#endif // CONFIGURATION_HPP

// #include "Context.hpp"

// #include "FpsCounter.hpp"
#ifndef FPS_COUNTER_HPP
#define FPS_COUNTER_HPP

#include <memory>

// #include "UiElement.hpp"
#ifndef UI_ELEMENT_HPP
#define UI_ELEMENT_HPP

class TCODConsole;

namespace engine
{
    struct pos_t;

    class UiElement
    {
        public:
            UiElement(bool visible = false);

            void Show();
            void Hide();

            bool IsVisible() const;

            virtual void Render(TCODConsole* console, const pos_t& pos) const = 0;

        protected:
            bool visible_;
    };
}

#endif // UI_ELEMENT_HPP


class TCODConsole;

namespace engine
{
    struct pos_t;

    class FpsCounter final : public UiElement
    {
        public:
            FpsCounter();

            void Render(TCODConsole* console, const pos_t& pos) const final;

        private:
            std::unique_ptr<TCODConsole> console_;
    };
}

#endif // FPS_COUNTER_HPP

// #include "WindowSize.hpp"
#ifndef WINDOW_SIZE_HPP
#define WINDOW_SIZE_HPP

namespace engine
{
    struct window_size_t
    {
        int width;
        int height;
    };
}

#endif // WINDOW_SIZE_HPP


namespace entt
{
    class dispatcher;
}

namespace engine
{
    enum State
    {
        INIT = 0,
        RUNNING,
        PAUSED,
        EXIT
    };

    class Engine
    {
        public:
            Engine(const Configuration& config);
            ~Engine();

            int Main();

            void ToggleFps();

            template<class T>
            void CreateContext();

        private:
            bool Init();
            void HandleInput();
            void Update();
            void Render() const;

            bool running_;

            Configuration config_;

            std::unique_ptr<Context> active_context_;
            std::unique_ptr<entt::dispatcher> event_bus_;

            FpsCounter fps_counter_;
    };

    template<class T>
    void Engine::CreateContext()
    {
        active_context_ = 
            std::make_unique<T>(
                window_size_t
                {
                    config_.width,
                    config_.height
                },
                *event_bus_);

        active_context_->RegisterEvents();
    }
}

#endif // ENGINE_HPP

// #include "EntityFactory.hpp"
#ifndef ENTITY_FACTORY_HPP
#define ENTITY_FACTORY_HPP

#include <vector>

#include <entt/entt.hpp>

namespace engine
{
    struct EntityFactory
    {
        template<typename T>
        static std::vector<T> GetEntity(const entt::registry& registry)
        {
            std::vector<T> list {};

            const auto view = registry.view<const T>();

            for(auto entity : view)
            {
                list.push_back(view.template get<const T>(entity) );
            }

            return list;
        }

        static entt::entity CreateEntity(entt::registry& registry)
        {
            return registry.create();
        }

        template<typename T>
        static entt::entity CreateEntity(entt::registry& registry, const T& component)
        {
            auto entity = registry.create();

            registry.emplace<T>(entity, component);

            return entity;
        }

        template<typename T, typename ...ARGS>
        static entt::entity CreateEntity(entt::registry& registry, const T& component, ARGS&... args)
        {
            auto entity = CreateEntity(registry, args...);

            registry.emplace<T>(entity, component);

            return entity;
        }

        static void Clear(entt::registry& registry)
        {
            registry.clear();
        }
    };
}

#endif // ENTITY_FACTORY_HPP

// #include "Map.hpp"
#ifndef MAP_HPP
#define MAP_HPP

#include <functional>
#include <vector>

// #include "WindowSize.hpp"


class TCODConsole;

namespace engine
{
    using generator = std::function<void()>;

    template<typename Tile>
    class Map
    {
        public:
            Map(const window_size_t& size);

            virtual void Generate(generator func) = 0;

            virtual void Render(TCODConsole* console) const = 0;

        protected:
            int width_;
            int height_;

            std::vector<Tile> tile_list_;
    };

    template<typename Tile>
    Map<Tile>::Map(const window_size_t& size)
        : width_(size.width), height_(size.height)
    {
    }
}

#endif // MAP_HPP

// #include "Position.hpp"
#ifndef POSITION_HPP
#define POSITION_HPP

namespace engine
{
    struct pos_t
    {
        int x;
        int y;
    };

    bool operator==(const pos_t& lhs, const pos_t& rhs);
    bool operator!=(const pos_t& lhs, const pos_t& rhs);
    pos_t operator+(const pos_t& lhs, const pos_t& rhs);
    void operator+=(pos_t& lhs, const pos_t& rhs);
}

#endif // POSITION_HPP

// #include "View.hpp"
#ifndef VIEW_HPP
#define VIEW_HPP

// #include "Position.hpp"

// #include "WindowSize.hpp"


class TCODConsole;

namespace engine
{
    class View
    {
        public:
            View(const window_size_t& view_area, const window_size_t& total_size, const pos_t& start_pos = pos_t { 0, 0 });
            virtual ~View() {}

            virtual void Update() = 0;

            virtual void Render(TCODConsole* parent, const pos_t& pos) const = 0;

        protected:
            window_size_t view_area_;
            window_size_t total_size_;
            pos_t start_pos_; // Upper-left corner of view, normally 0,0
    };
}

#endif // VIEW_HPP

// #include "WindowSize.hpp"


// UI
// #include "UiElement.hpp"


// Builtins
// #include "FpsCounter.hpp"

