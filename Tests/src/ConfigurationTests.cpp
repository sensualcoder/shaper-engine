#include <catch.hpp>

#include "Configuration.hpp"

namespace engine
{
    TEST_CASE("Configuration serialization")
    {
        Configuration test_config
        {
            80,
            25,
            "test",
            false
        };

        SECTION("Test save and load")
        {
            SaveConfig("test.json", test_config);

            auto new_config = LoadConfig("test.json");

            REQUIRE(test_config.width == new_config.width);
            REQUIRE(test_config.height == new_config.height);
            REQUIRE(test_config.title == new_config.title);
            REQUIRE(test_config.fullscreen == new_config.fullscreen);
        }
    }
}