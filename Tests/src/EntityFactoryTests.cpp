#include <catch.hpp>

#include <entt/entt.hpp>

#include "EntityFactory.hpp"
#include "Position.hpp"

namespace engine
{   
    TEST_CASE("Entity creation")
    {
        entt::registry registry;

        SECTION("Create empty entity")
        {
            auto entity = EntityFactory::CreateEntity(registry);

            REQUIRE(registry.valid(entity));
        }

        SECTION("Create entity with one component")
        {
            pos_t test_pos { 5, 5 };

            auto entity = EntityFactory::CreateEntity(registry, test_pos);

            REQUIRE(registry.valid(entity));

            registry.view<pos_t>().each(
                [&test_pos]
                ([[maybe_unused]] auto entity, auto& pos)
                {
                    REQUIRE(test_pos == pos);
                });
        }

        SECTION("Create entity with multiple components")
        {
            pos_t test_pos { 1, 1 };
            int test_int = 5;
            char test_char = 'a';

            auto entity = EntityFactory::CreateEntity(
                registry, 
                test_pos,
                test_int,
                test_char);

            REQUIRE(registry.valid(entity));

            registry.view<pos_t, int, char>().each(
                [&test_pos, &test_int, &test_char]
                ([[maybe_unused]] auto entity, auto& pos, auto& mint, auto& mchar)
                {
                    REQUIRE(test_pos == pos);
                    REQUIRE(test_int == mint);
                    REQUIRE(test_char == mchar);
                });
        }

        SECTION("Create a ton of entities")
        {
            int entity_count = 1000;

            pos_t test_pos { 1, 1 };

            for(int i = 0; i < entity_count; ++i)
            {
                auto entity = EntityFactory::CreateEntity(
                    registry, 
                    test_pos);

                REQUIRE(registry.valid(entity));
            }

            int count = 0;

            registry.view<pos_t>().each(
                [&test_pos, &count]
                ([[maybe_unused]] auto entity, auto& pos)
                {
                    REQUIRE(pos == test_pos);

                    ++count;
                });

            REQUIRE(count == entity_count);
        }
    }
}
