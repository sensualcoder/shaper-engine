#ifndef CONTEXT_TEST_HPP
#define CONTEXT_TEST_HPP

#include <memory>

#include <entt/entt.hpp>
#include <libtcod/libtcod.hpp>

#include "Context.hpp"
#include "Events.hpp"
#include "View.hpp"

class TCODConsole;

namespace engine
{   
    struct window_size_t;

    class ContextTest final : public Context
    {
        public:
            ContextTest(const window_size_t& size, entt::dispatcher& dispatcher);

            void RegisterEvents() override;

            void CreateEntity(const CreateEntityEvent& event);
            void ClearEntities(const ClearEntityEvent& event);

            void HandleInput() override;

            void Update() override;

            void Render(TCODConsole* parent) const override;

        private:
            std::unique_ptr<TCODConsole> console_;

            entt::registry registry_;

            entt::dispatcher& dispatcher_;

            std::vector<std::unique_ptr<View> > views_;

            std::function<void(const TCOD_key_t&, entt::dispatcher&)> handler_;
    };
}

#endif // CONTEXT_TEST_HPP
