#ifndef DRIVER_HPP
#define DRIVER_HPP

namespace engine
{
    class Driver
    {
        public:
            static int Main();
    };
}

#endif // DRIVER_HPP
