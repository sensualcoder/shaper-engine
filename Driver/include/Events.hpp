#ifndef EVENTS_HPP
#define EVENTS_HPP

#include "Position.hpp"

namespace engine
{
    struct CreateEntityEvent
    {
        CreateEntityEvent(pos_t&& pos) : pos(pos) {}

        pos_t pos;
    };

    struct ClearEntityEvent
    {
    };
}

#endif // EVENTS_HPP
