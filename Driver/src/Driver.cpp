#include "Driver.hpp"

#include <string>

#include <entt/entt.hpp>
#include <libtcod/libtcod.hpp>

#include "Configuration.hpp"
#include "ContextTest.hpp"
#include "Engine.hpp"
#include "Events.hpp"

namespace engine
{
    const Configuration DEFAULT_CONFIG
    {
        80,
        25,
        "Engine Test",
        false
    };

    int Driver::Main()
    {
        Configuration config{};

        try
        {
            config = LoadConfig("config/config.json");
        }
        catch(std::exception& e)
        {
            config = DEFAULT_CONFIG;
        }

        Engine engine(config);

        engine.CreateContext<ContextTest>();

        return engine.Main();
    }
}
