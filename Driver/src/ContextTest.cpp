#include "ContextTest.hpp"

#include <libtcod/libtcod.hpp>

#include "BasicView.hpp"
#include "EntityFactory.hpp"
#include "WindowSize.hpp"

namespace engine
{
    auto KeypressHandler = [](const TCOD_key_t& key, entt::dispatcher& dispatcher)
    {
        switch(key.vk)
        {
            case TCODK_CHAR:
            {
                switch(key.c)
                {
                    case 'n':
                        dispatcher.enqueue<CreateEntityEvent>( pos_t { 10, 10 } );
                        break;
                    case 'c':
                        dispatcher.enqueue<ClearEntityEvent>();
                        break;
                    default:
                        break;
                }
            }
            default:
                break;
        }
    };

    ContextTest::ContextTest(const window_size_t& size, entt::dispatcher& dispatcher)
        : console_(new TCODConsole(size.width, size.height) ),
        dispatcher_(dispatcher),
        handler_(KeypressHandler)
    {
        views_.push_back(
            std::make_unique<BasicView>(
                window_size_t { 30, 25 },
                window_size_t { 30, 100 },
                registry_
            ) 
        );
    }

    void ContextTest::RegisterEvents()
    {
        dispatcher_
            .sink<CreateEntityEvent>()
            .connect<&ContextTest::CreateEntity>(this);

        dispatcher_
            .sink<ClearEntityEvent>()
            .connect<&ContextTest::ClearEntities>(this);
    }

    void ContextTest::CreateEntity(const CreateEntityEvent& event)
    {
        EntityFactory::CreateEntity(registry_, event.pos);
    }

    void ContextTest::ClearEntities(const ClearEntityEvent&)
    {
        EntityFactory::Clear(registry_);
    }

    void ContextTest::HandleInput()
    {
        TCOD_key_t key;

        TCODSystem::checkForEvent(TCOD_KEY_PRESSED, &key, nullptr);

        if(key.vk == TCODK_NONE)
        {
            return;
        }

        if(handler_) handler_(key, dispatcher_);
    }

    void ContextTest::Update()
    {
        dispatcher_.update();

        for(auto view = views_.begin(); view != views_.end(); ++view)
        {
            (*view)->Update();
        }
    }

    void ContextTest::Render(TCODConsole* parent) const
    {
        console_->clear();

        for(auto view = views_.begin(); view != views_.end(); ++view)
        {
            (*view)->Render(
                console_.get(), 
                pos_t { 0, 0 }
            );
        }

        parent->blit(
            console_.get(),
            0,
            0,
            console_->getWidth(),
            console_->getHeight(),
            parent,
            0,
            0
        );
    }
}
