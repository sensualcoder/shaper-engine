#include "Position.hpp"

namespace engine
{
    bool operator==(const pos_t& lhs, const pos_t& rhs)
    {
        return lhs.x == rhs.x
            && lhs.y == rhs.y;
    }

    bool operator!=(const pos_t& lhs, const pos_t& rhs)
    {
        return !(lhs == rhs);
    }

    pos_t operator+(const pos_t& lhs, const pos_t& rhs)
    {
        return pos_t { lhs.x + rhs.x, lhs.y + rhs.y };
    }

    void operator+=(pos_t& lhs, const pos_t& rhs)
    {
        lhs = lhs + rhs;
    }
}
