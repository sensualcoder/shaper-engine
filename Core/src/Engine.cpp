#include "Engine.hpp"

#include <entt/entt.hpp>
#include <libtcod/libtcod.hpp>

#include "Position.hpp"

namespace engine
{
    // Public methods
    Engine::Engine(const Configuration& config)
        : running_(false), 
        config_(config),
        active_context_(nullptr),
        event_bus_(new entt::dispatcher)
    {
    }

    int Engine::Main()
    {
        if(!this->Init() )
        {
            return -1;
        }

        while(running_ && !TCODConsole::isWindowClosed() )
        {
            this->HandleInput();
            this->Update();
            this->Render();
        }

        TCOD_quit();

        return 0;
    }

    void Engine::ToggleFps()
    {
        fps_counter_.IsVisible() 
            ? fps_counter_.Hide()
            : fps_counter_.Show();
    }

    // Private methods
    bool Engine::Init()
    {
        TCODConsole::initRoot(
            config_.width,
            config_.height,
            config_.title.c_str(),
            config_.fullscreen,
            TCOD_RENDERER_OPENGL2
        );

        TCODSystem::setFps(60);

        running_ = true;

        return running_;
    }
    
    void Engine::HandleInput()
    {
        active_context_->HandleInput();
    }
    
    void Engine::Update()
    {
        active_context_->Update();
    }
    
    void Engine::Render() const
    {
        auto root = TCODConsole::root;

        root->clear();

        if(active_context_)
        {
            active_context_->Render(root);
        }

        if(fps_counter_.IsVisible() )
        {
            fps_counter_.Render(
                root,
                pos_t
                {
                    config_.width - 10,
                    config_.height - 1
                });
        }

        root->flush();
    }
}
