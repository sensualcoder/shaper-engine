#include "Console.hpp"

#include <libtcod/libtcod.hpp>

#include "Position.hpp"
#include "WindowSize.hpp"

namespace engine
{
    Console::Console(const window_size_t& size)
        : console_(new TCODConsole(size.width, size.height) )
    {
    }

    TCODConsole* Console::getConsole()
    {
        return console_.get();
    }

    void Console::setDefaultBackground(const TCODColor& color)
    {
        console_->setDefaultBackground(color);
    }

    void Console::setDefaultForeground(const TCODColor& color)
    {
        console_->setDefaultForeground(color);
    }

    void Console::blit(Console* parent, const pos_t& pos)
    {
        console_->blit(
            console_.get(),
            0,
            0,
            console_->getWidth(),
            console_->getHeight(),
            parent->getConsole(),
            pos.x,
            pos.y
        );
    }

    void Console::flush()
    {
        console_->flush();
    }

    void Console::clear()
    {
        console_->clear();
    }
}
