#include "Configuration.hpp"

#include <fstream>

#include <cereal/archives/json.hpp>

namespace engine
{
    template<class Archive>
    void serialize(Archive& archive, Configuration& config)
    {
        archive(cereal::make_nvp("Screen Width", config.width),
                cereal::make_nvp("Screen Height", config.height),
                cereal::make_nvp("Title", config.title),
                cereal::make_nvp("Fullscreen", config.fullscreen) );
    }

    void SaveConfig(std::string filename, Configuration& config)
    {
        std::ofstream out(filename);
        cereal::JSONOutputArchive archive(out);

        archive(cereal::make_nvp("Config", config) );
    }

    Configuration LoadConfig(std::string filename)
    {
        std::ifstream in(filename);
        cereal::JSONInputArchive archive(in);

        Configuration config;

        archive(config);

        return config;
    }
}
