#ifndef ENTITY_FACTORY_HPP
#define ENTITY_FACTORY_HPP

#include <vector>

#include <entt/entt.hpp>

namespace engine
{
    struct EntityFactory
    {
        template<typename T>
        static std::vector<T> GetEntity(const entt::registry& registry)
        {
            std::vector<T> list {};

            const auto view = registry.view<const T>();

            for(auto entity : view)
            {
                list.push_back(view.template get<const T>(entity) );
            }

            return list;
        }

        static entt::entity CreateEntity(entt::registry& registry)
        {
            return registry.create();
        }

        template<typename T>
        static entt::entity CreateEntity(entt::registry& registry, const T& component)
        {
            auto entity = registry.create();

            registry.emplace<T>(entity, component);

            return entity;
        }

        template<typename T, typename ...ARGS>
        static entt::entity CreateEntity(entt::registry& registry, const T& component, ARGS&... args)
        {
            auto entity = CreateEntity(registry, args...);

            registry.emplace<T>(entity, component);

            return entity;
        }

        static void Clear(entt::registry& registry)
        {
            registry.clear();
        }
    };
}

#endif // ENTITY_FACTORY_HPP
