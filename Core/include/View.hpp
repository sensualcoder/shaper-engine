#ifndef VIEW_HPP
#define VIEW_HPP

class TCODConsole;

namespace engine
{
    struct pos_t;

    class View
    {
        public:
            virtual ~View() = default;

            virtual void Update() = 0;

            virtual void Render(TCODConsole* parent, const pos_t& pos) const = 0;
    };
}

#endif // VIEW_HPP
