#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <memory>

class TCODColor;
class TCODConsole;

namespace engine
{
    struct pos_t;
    struct window_size_t;

    class Console
    {
        public:
            Console(const window_size_t& size);

            TCODConsole* getConsole();

            void setDefaultBackground(const TCODColor& color);
            void setDefaultForeground(const TCODColor& color);

            void blit(Console* parent, const pos_t& pos);
            void flush();

            void clear();

        private:
            std::unique_ptr<TCODConsole> console_;
    };
}

#endif // CONSOLE_HPP
