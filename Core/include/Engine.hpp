#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <memory>

#include "Configuration.hpp"
#include "Context.hpp"
#include "FpsCounter.hpp"
#include "WindowSize.hpp"

namespace entt
{
    class dispatcher;
}

namespace engine
{
    enum State
    {
        INIT = 0,
        RUNNING,
        PAUSED,
        EXIT
    };

    class Engine
    {
        public:
            Engine(const Configuration& config);

            int Main();

            void ToggleFps();

            template<class T>
            void CreateContext();

        private:
            bool Init();
            void HandleInput();
            void Update();
            void Render() const;

            bool running_;

            Configuration config_;

            std::unique_ptr<Context> active_context_;
            std::unique_ptr<entt::dispatcher> event_bus_;

            FpsCounter fps_counter_;
    };

    template<class T>
    void Engine::CreateContext()
    {
        active_context_ = 
            std::make_unique<T>(
                window_size_t
                {
                    config_.width,
                    config_.height
                },
                *event_bus_);

        active_context_->RegisterEvents();
    }
}

#endif // ENGINE_HPP
