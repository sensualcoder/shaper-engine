#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>

namespace engine
{
    struct Configuration
    {
        int width;
        int height;
        std::string title;
        bool fullscreen;
    };

    void SaveConfig(std::string filename, Configuration& config);
    Configuration LoadConfig(std::string filename);
}

#endif // CONFIGURATION_HPP
