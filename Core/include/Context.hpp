#ifndef CONTEXT_HPP
#define CONTEXT_HPP

class TCODConsole;

namespace engine
{
    class Context
    {
        public:
            virtual ~Context() = default;

            virtual void RegisterEvents() = 0;

            virtual void HandleInput() = 0;

            virtual void Update() = 0;

            virtual void Render(TCODConsole* parent) const = 0;
    };
}

#endif // CONTEXT_HPP
