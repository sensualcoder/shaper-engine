#ifndef MAP_HPP
#define MAP_HPP

#include <functional>
#include <vector>

#include "WindowSize.hpp"

class TCODConsole;

namespace engine
{
    using generator = std::function<void()>;

    template<typename Tile>
    class Map
    {
        public:
            Map(const window_size_t& size);

            virtual void Generate(generator func) = 0;

            virtual void Render(TCODConsole* console) const = 0;

        protected:
            int width_;
            int height_;

            std::vector<Tile> tile_list_;
    };

    template<typename Tile>
    Map<Tile>::Map(const window_size_t& size)
        : width_(size.width), height_(size.height)
    {
    }
}

#endif // MAP_HPP
