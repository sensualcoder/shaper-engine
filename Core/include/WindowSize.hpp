#ifndef WINDOW_SIZE_HPP
#define WINDOW_SIZE_HPP

namespace engine
{
    struct window_size_t
    {
        int width;
        int height;
    };
}

#endif // WINDOW_SIZE_HPP
