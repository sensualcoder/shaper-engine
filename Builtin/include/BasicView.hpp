#ifndef BASIC_VIEW_HPP
#define BASIC_VIEW_HPP

#include <memory>
#include <vector>

#include <entt/entt.hpp>

#include "Position.hpp"
#include "View.hpp"
#include "WindowSize.hpp"

class TCODConsole;

namespace engine
{
    class BasicView final : public View
    {
        public:
            BasicView(const window_size_t& view_area, const window_size_t& total_size, entt::registry& registry);

            void MoveUp();
            void MoveDown();

            void Update() override;

            void Render(TCODConsole* parent, const pos_t& pos) const override;

        private:
            pos_t start_pos_;
            window_size_t view_area_;
            window_size_t total_size_;

            std::unique_ptr<TCODConsole> console_;

            std::vector<pos_t> entities_;

            entt::registry& registry_;
    };
}

#endif // SCROLLING_VIEW_HPP
