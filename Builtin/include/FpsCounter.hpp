#ifndef FPS_COUNTER_HPP
#define FPS_COUNTER_HPP

#include <memory>

#include "UiElement.hpp"

class TCODConsole;

namespace engine
{
    struct pos_t;

    class FpsCounter final : public UiElement
    {
        public:
            FpsCounter();
            ~FpsCounter() = default;

            FpsCounter(FpsCounter&& src) = default;
            FpsCounter& operator=(FpsCounter&& src) = default;

            void Render(TCODConsole* console, const pos_t& pos) const final;

        private:
            std::unique_ptr<TCODConsole> console_;
    };
}

#endif // FPS_COUNTER_HPP
