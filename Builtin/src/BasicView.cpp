#include "BasicView.hpp"

#include <string>

#include <fmt/core.h>
#include <libtcod/libtcod.hpp>

#include "EntityFactory.hpp"

namespace engine
{
    BasicView::BasicView(const window_size_t& view_area, const window_size_t& total_size, entt::registry& registry)
        : start_pos_(pos_t { 0, 0 }),
            view_area_(view_area),
            total_size_(total_size),
            console_(new TCODConsole(total_size.width, total_size.height) ),
            registry_(registry)
    {
    }

    void BasicView::Update()
    {
        entities_ = EntityFactory::GetEntity<pos_t>(registry_);
    }

    void BasicView::Render(TCODConsole* parent, const pos_t& pos) const
    {
        console_->clear();

        for(std::size_t i = 0; i < entities_.size(); ++i)
        {
            std::string message = 
                fmt::format("Entity {}, position: {}, {}",
                    i,
                    entities_[i].x,
                    entities_[i].y);

            console_->printf(
                0,
                i,
                "%s",
                message.c_str()
            );
        }

        parent->blit(
            console_.get(),
            start_pos_.x,
            start_pos_.y,
            view_area_.width,
            view_area_.height,
            parent,
            pos.x,
            pos.y
        );
    }
}
