#include "FpsCounter.hpp"

#include <libtcod/libtcod.hpp>

#include "Position.hpp"
#include "WindowSize.hpp"

namespace engine
{
    FpsCounter::FpsCounter()
        : UiElement(true),
        console_(new TCODConsole(9, 1) )
    {
        console_->setDefaultForeground(TCODColor::white);
        console_->setDefaultBackground(TCODColor::azure);
    }

    void FpsCounter::Render(TCODConsole* console, const pos_t& pos) const
    {
        if(!visible_)
        {
            return;
        }

        console_->clear();

        console_->printf(
            1, 
            0,
            "FPS: %i",
            TCODSystem::getFps() );

        TCODConsole::blit(
            console_.get(),
            0, 
            0, 
            console_->getWidth(), 
            console_->getHeight(),
            console,
            pos.x,
            pos.y
        );
    }
}
